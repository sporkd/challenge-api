import { Low, JSONFile } from "lowdb";
import { dirname, join } from "path";
import { fileURLToPath } from "url";

let store;

const __dirname = dirname(fileURLToPath(import.meta.url));

export async function initDatastore() {
  const file = join(__dirname, "../data.json");
  const adapter = new JSONFile(file);
  store = new Low(adapter);

  await store.read();

  store.data ||= { transactions: [] };
  await store.write();
}

export const datastore = () => store;
