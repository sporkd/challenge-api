import { app } from "./app.js";
import { initDatastore } from "./datastore.js";

initDatastore();
app.listen(app.get("port"));
console.log("API running on port", app.get("port"));
