import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import transactions from "./routes/transactions.js";

const app = express();

app.use(cors())
app.set("port", process.env.PORT || 3000);

// middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// routes
app.use(transactions);

export { app };
