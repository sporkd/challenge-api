import { datastore } from "../datastore.js";
import { check, validationResult } from "express-validator";

export const getTransactions = (req, res) => {
  const transactions = datastore().data.transactions;
  console.log(transactions);
  res.json(transactions);
};

export const createTransaction = async (req, res) => {
  await check('type').exists().isIn(['credit', 'debit'])
    .withMessage('Must be either credit or debit')
    .run(req);
  await check('amount').exists().isFloat({min: 0})
    .withMessage('Must be a positive numeric value')
    .run(req);
  await check('description').exists().run(req);

  const validationResults = await validationResult(req);
  if (!validationResults.isEmpty()) {
    return res.status(400).send(
      `error: ${JSON.stringify(validationResults.array())}`
    );
  }

  const transaction = {
    type: req.body.type,
    amount: req.body.amount,
    description: req.body.description
  };

  try {
    await writeTransaction(transaction)
    res.json(transaction);
  } catch (error) {
    return res.status(500).send(error);
  }
};

const writeTransaction = async (transaction) => {
  try {
    const store = datastore();
    store.data.transactions.push(transaction);
    await store.write();
    return transaction;
  } catch (error) {
    return error;
  }
};

export const batchTransactions = async (req, res) => {
  const transactions = req.body.transactions;
  let results = [];

  transactions.forEach(async (transaction) => {
    console.log("writing transaction ", transaction);
    let result = await writeTransaction(transaction);
    results.push(result);
  });

  return res.send(results);
}

export const balance = async (req, res) => {
  const transactions = datastore().data.transactions;
  const balance = transactions.reduce(function (acc, obj) {
    if (typeof(obj.amount) !== 'number') {
      obj.amount = parseFloat(obj.amount);
    }
    if (obj.type === 'credit') {
      return acc + obj.amount;
    }
    else if (obj.type === 'debit') {
      return acc - obj.amount;
    }
    else {
      return acc;
    }
  }, 0);
  res.json(balance);
};
