import { Router } from "express";
const router = Router();

import {
  getTransactions,
  createTransaction,
  batchTransactions,
  balance,
} from "../controllers/transactions.controller.js";

router.get("/transactions", getTransactions);

router.post("/transactions", createTransaction);

router.post("/transactions/batch", batchTransactions);

router.get("/transactions/balance", balance);

export default router;
